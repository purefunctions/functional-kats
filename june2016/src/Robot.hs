module Robot where

import Control.Monad (foldM)

data Alignment = North | East | West | South deriving Show

data Rotation = CW | CCW deriving Show

type Position = (Integer, Integer)

type RobotState = (Position, Alignment)

type UserCommand = Char

type UserCommands = [UserCommand]

-- Advance in the same direction
advance :: RobotState -> RobotState
advance ((x,y), North) = ((x, y+1), North)
advance ((x,y), East) = ((x+1, y), East)
advance ((x,y), West) = ((x-1, y), West)
advance ((x,y), South) = ((x, y-1), South)

--  turn in position
turn :: RobotState -> Rotation -> RobotState
turn ((x,y), North) CW = ((x,y), East)
turn ((x,y), North) CCW = ((x,y), West)
turn ((x,y), East) CW = ((x,y), South)
turn ((x,y), East) CCW = ((x,y), North)
turn ((x,y), West) CW = ((x,y), North)
turn ((x,y), West) CCW = ((x,y), South)
turn ((x,y), South) CW = ((x,y), West)
turn ((x,y), South) CCW = ((x,y), East)

-- Moves based on user command (character)
move :: RobotState -> UserCommand -> Either String RobotState
move s 'L' = Right $ turn s CCW
move s 'R' = Right $ turn s CW
move s 'A' = Right $ advance s
move _ c = Left $ "Unknown command encountered: " ++ [c]

multiMove :: RobotState -> UserCommands -> Either String RobotState
multiMove initState commands = foldM move initState commands
