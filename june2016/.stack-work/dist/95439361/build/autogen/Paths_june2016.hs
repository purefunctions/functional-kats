module Paths_june2016 (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "C:\\jsiva\\sw\\code\\functional-kats\\june2016\\.stack-work\\install\\59a52906\\bin"
libdir     = "C:\\jsiva\\sw\\code\\functional-kats\\june2016\\.stack-work\\install\\59a52906\\lib\\i386-windows-ghc-7.10.3\\june2016-0.1.0.0-Ehw6cWDnGgy44iCl0ygU79"
datadir    = "C:\\jsiva\\sw\\code\\functional-kats\\june2016\\.stack-work\\install\\59a52906\\share\\i386-windows-ghc-7.10.3\\june2016-0.1.0.0"
libexecdir = "C:\\jsiva\\sw\\code\\functional-kats\\june2016\\.stack-work\\install\\59a52906\\libexec"
sysconfdir = "C:\\jsiva\\sw\\code\\functional-kats\\june2016\\.stack-work\\install\\59a52906\\etc"

getBinDir, getLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "june2016_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "june2016_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "june2016_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "june2016_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "june2016_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "\\" ++ name)
