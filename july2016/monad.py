from functools import reduce

viper = {
    "name": "Mike Metcalf",
    "contact": {
        "address": {
            "street": "1 Miramar Way",
            "city": "San Diego",
            "zip": 92126
        },
        "phone": "1221222",
        "mobile": "8586661111"
    }
}

maverick = {
    "name": "Pete Mitchell",
    "commander": viper,
    "contact": {
        "address": {
            "street": "123 Miramar Way",
            "city": "San Diego",
            "zip": 92126
        },
        "phone": "88586667777",
    }
}

iceman = {
    "name": "Tom Kazanski",
    "commander": viper,
    "contact": {
        "address": {
            "street": "521 Miramar Way",
            "city": "San Diego",
            "zip": 92126
        },
        "phone": "1221222",
        "mobile": "2224433"
    }
}

charlie = {
    "name": "Charlotte Blackwood",
}


def get_commander_city(person):
    return person.get("commander").get("contact").get("address").get("city")


# Tony Hoare: Null reference billion dollar mistake
def get_commander_city_excepted(person):
    try:
        return person["commander"]["contact"]["address"]["city"]
    except (AttributeError, KeyError):
        return None


def get_commander(person):
    return person.get("commander")


def get_contact(person):
    return person.get("contact")


def get_address(contact):
    return contact.get("address")


def get_city(address):
    return address.get("city")


def get_commander_city_checked(person):
    if person is not None:
        commander = get_commander(person)
        if commander is not None:
            contact = get_contact(commander)
            if contact is not None:
                address = get_address(contact)
                if address is not None:
                    return get_city(address)
    return None


def call_if_not_none(fn, value):
    if value is not None:
        return fn(value)
    return None


def get_commander_city_abstracted1(person):
    return call_if_not_none(
        lambda x: call_if_not_none(
            lambda y: call_if_not_none(
                lambda z: call_if_not_none(
                   lambda a: get_city(a),
                   get_address(z),
                ),
                get_contact(y),
            ),
            get_commander(x),
        ),
        person,
    )


pass_status = {
    "status": True,
    "value": "Some value that can be of any type"
}

fail_status = {
    "status": False,
    "error": "A string representing error"
}

driving = {
    "header": "some header that must be present",
    "payload": {
        "commands": {
            "pre": {
                "name": "start engine"
            },
            "normal": {
                "name": "drive"
            },
            "post": {
                "name": "stop engine"
            }
        }
    }
}

riding = {
    "header": "some header that must be present",
    "payload": {
        "commands": {
            "normal": {
                "name": "ride"
            },
        }
    }
}

sleeping = {
    "header": "some header that must be present",
    "payload": {
        "commands": {
            "pre": {
                "name": "close eyes"
            },
            "post": {
                "name": "open eyes"
            },
        }
    }
}


def get_payload(message):
    if "header" in message:
        # do some processing of header
        if "payload" in message:
            return {"status": True, "value": message["payload"]}
        else:
            return {"status": False, "error": "No payload in message"}
    else:
        return {"status": False, "error": "No header in message"}


def get_commands(payload):
    if "commands" in payload:
        return {"status": True, "value": payload["commands"]}
    else:
        return {"status": False, "value": "No commands section in payload"}


def get_normal_command(commands):
    if "normal" in commands:
        return {"status": True, "value": commands["normal"]}
    else:
        return {"status": False, "value": "No normal command in commands"}


def get_command_name(command):
    if "name" in command:
        return {"status": True, "value": command["name"]}
    else:
        return {"status": False, "value": "No command name specified for command"}


def get_normal_command_name_from_message(message):
    payload = get_payload(message)
    if payload["status"]:
        commands = get_commands(payload["value"])
        if commands["status"]:
            normal_command = get_normal_command(commands["value"])
            if normal_command["status"]:
                return get_command_name(normal_command["value"])
            else:
                return normal_command
        else:
            return commands
    else:
        return payload


def call_upon_valid_value(fn, item):
    if item["status"]:
        return fn(item["value"])
    else:
        return item


def get_normal_command_name_from_message_abstracted1(message):
    return call_upon_valid_value(
        lambda x: call_upon_valid_value(
            lambda y: call_upon_valid_value(
                lambda z: call_upon_valid_value(
                    lambda a: get_command_name(a),
                    get_normal_command(z)
                ),
                get_commands(y),
            ),
            get_payload(x)
        ),
        {"status": True, "value": message}
    )


def identity(a):
    return a


def wrapped_in_success(x):
    return {"status": True, "value": x}


none_stepper_def = {
    "stepper": call_if_not_none,
    "wrapper": identity
}

status_stepper_def = {
    "stepper": call_upon_valid_value,
    "wrapper": wrapped_in_success
}


def link(data, steps, stepper_def):
    def reduction_fn(acc_data, fn):
        return stepper_def["stepper"](
                fn,
                acc_data
        )

    return reduce(
        reduction_fn,                          # reduction function
        steps,                                 # sequence of steps
        stepper_def["wrapper"](data)           # initial value
    )


def get_normal_command_name_from_message_abstracted2(message):
    return link(message, [get_payload, get_commands, get_normal_command, get_command_name], status_stepper_def)


def get_commander_city_abstracted2(person):
    return link(person, [get_commander, get_contact, get_address, get_city], none_stepper_def)
